/* eslint-disable no-undef */
/* eslint-disable @typescript-eslint/no-var-requires */
const extraRuns = require('../src/server/3-runs-conceded-per-team');

const sampleMatch = [
  {
    id: 1,
    season: 2016,
  },
  {
    id: 2,
    season: 2008,
  },
  {
    id: 3,
    season: 2016,
  },
];
const sampleDeliveries = [
  {
    match_id: 1,
    bowling_team: 'MI',
    extra_runs: 14,
  },
  {
    match_id: 1,
    bowling_team: 'MI',
    extra_runs: 17,
  },
  {
    match_id: 1,
    bowling_team: 'SRH',
    extra_runs: 10,
  },
  {
    match_id: 2,
    bowling_team: 'MI',
    extra_runs: 4,
  },
  {
    match_id: 3,
    bowling_team: 'MI',
    extra_runs: 19,
  },
];
const sampleOutput = {
  MI: 50,
  SRH: 10,
};

test('Extra Runs per Team in the Year 2016', () => {
  expect(extraRuns(sampleMatch, sampleDeliveries)).toMatchObject(sampleOutput);
});
