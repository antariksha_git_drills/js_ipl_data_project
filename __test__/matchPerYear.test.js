/* eslint-disable no-undef */
/* eslint-disable @typescript-eslint/no-var-requires */

const matchPerYear = require('../src/server/1-matches-peryear.js');

const sampleMatches = [
  {
    season: 2008,
  },
  {
    season: 2009,
  },
  {
    season: 2009,
  },
  {
    season: 2010,
  },
];
const sampleOutput = {
  2008: 1,
  2009: 2,
  2010: 1,
};

test('Matches per season', () => {
  expect(matchPerYear(sampleMatches)).toEqual(sampleOutput);
});
