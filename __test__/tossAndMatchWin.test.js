/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable no-undef */
const tossWinMatchWin = require('../src/server/5-TossWinMatchWin.js');

const sampleMatch = [
  { toss_winner: 'A', winner: 'B' },
  { toss_winner: 'C', winner: 'C' },
  { toss_winner: 'D', winner: 'C' },
  { toss_winner: 'D', winner: 'D' },
  { toss_winner: 'B', winner: 'B' },
  { toss_winner: 'C', winner: 'C' },
];

const sampleOutput = { C: 2, D: 1, B: 1 };
test('Teams who won both toss and maych', () => {
  expect(tossWinMatchWin(sampleMatch)).toMatchObject(sampleOutput);
});
