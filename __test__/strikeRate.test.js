/* eslint-disable no-undef */
/* eslint-disable @typescript-eslint/no-var-requires */
const strikeRate = require('../src/server/7-StrikeRatePerSeason.js');

const sampleMatches = [
  { id: 1, season: '2009' },
  { id: 2, season: '2011' },
  { id: 3, season: '2015' },
];

const sampleDeliveries = [
  {
    match_id: 1,
    batsman: 'A',
    batsman_runs: '1',
    wide_runs: '0',
    noball_runs: '0',
  },
  {
    match_id: 1,
    batsman: 'B',
    batsman_runs: '2',
    wide_runs: '0',
    noball_runs: '0',
  },
  {
    match_id: 2,
    batsman: 'C',
    batsman_runs: '3',
    wide_runs: '0',
    noball_runs: '0',
  },
  {
    match_id: 2,
    batsman: 'D',
    batsman_runs: '4',
    wide_runs: '0',
    noball_runs: '0',
  },
  {
    match_id: 3,
    batsman: 'E',
    batsman_runs: '4',
    wide_runs: '0',
    noball_runs: '0',
  },
  {
    match_id: 3,
    batsman: 'F',
    batsman_runs: '3',
    wide_runs: '0',
    noball_runs: '0',
  },
  {
    match_id: 4,
    batsman: 'G',
    total_runs: '4',
    wide_runs: '0',
    noball_runs: '0',
  },
  {
    match_id: 5,
    batsman: 'H',
    total_runs: '1',
    wide_runs: '0',
    noball_runs: '0',
  },
  {
    match_id: 5,
    batsman: 'I',
    total_runs: '2',
    wide_runs: '0',
    noball_runs: '0',
  },
];

const sampleOutput = {
  2009: {
    A: { runs: 1, ballsFaced: 1, strikeRate: '100.00' },
    B: { runs: 2, ballsFaced: 1, strikeRate: '200.00' },
  },
  2011: {
    C: { runs: 3, ballsFaced: 1, strikeRate: '300.00' },
    D: { runs: 4, ballsFaced: 1, strikeRate: '400.00' },
  },
  2015: {
    E: { runs: 4, ballsFaced: 1, strikeRate: '400.00' },
    F: { runs: 3, ballsFaced: 1, strikeRate: '300.00' },
  },
};
test('Strike Rate', () => {
  expect(strikeRate(sampleMatches, sampleDeliveries)).toMatchObject(
    sampleOutput,
  );
});
