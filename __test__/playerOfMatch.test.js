/* eslint-disable no-undef */
/* eslint-disable @typescript-eslint/no-var-requires */
const playerOfMatch = require('../src/server/6-PlayerOfMatch.js');

const sampleMatch = [
  { season: 2008, player_of_match: 'A' },
  { season: 2008, player_of_match: 'B' },
  { season: 2008, player_of_match: 'A' },
  { season: 2012, player_of_match: 'D' },
  { season: 2012, player_of_match: 'D' },
  { season: 2012, player_of_match: 'C' },
  { season: 2012, player_of_match: 'C' },
  { season: 2012, player_of_match: 'C' },
];

const sampleOutput = {
  2008: 'A',
  2012: 'C',
};
test('Most Player Of Match Each Year', () => {
  expect(playerOfMatch(sampleMatch)).toMatchObject(sampleOutput);
});
