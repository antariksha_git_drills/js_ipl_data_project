/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable no-undef */
const dismiss = require('../src/server/8-HighestDismissal.js');

const sampleDelivery = [
  {
    player_dismissed: 'A',
    bowler: 'B',
  },
  {
    player_dismissed: 'C',
    bowler: 'D',
  },
  {
    player_dismissed: 'A',
    bowler: 'B',
  },
  {
    player_dismissed: 'A',
    bowler: 'D',
  },
  {
    player_dismissed: 'B',
    bowler: 'C',
  },
  {
    player_dismissed: 'B',
    bowler: 'D',
  },
  {
    player_dismissed: 'A',
    bowler: 'B',
  },
];

const sampleOutput = {
  'A,B': 3,
};
test('Most Dismissal Pair', () => {
  expect(dismiss(0, sampleDelivery)).toMatchObject(sampleOutput);
});
