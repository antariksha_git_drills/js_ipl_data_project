/* eslint-disable no-undef */
/* eslint-disable @typescript-eslint/no-var-requires */
const matchWon = require('../src/server/2-matches-won-per-team-per-year.js');

const sampleMatch = [
  {
    season: 2016,
    winner: 'MI',
  },
  {
    season: 2008,
    winner: 'KKR',
  },
  {
    season: 2008,
    winner: 'RCB',
  },
  {
    season: 2010,
    winner: 'KKR',
  },
  {
    season: 2008,
    winner: 'KKR',
  },
  {
    season: 2017,
    winner: 'CSK',
  },
];
const sampleOutput = {
  2016: {
    MI: 1,
  },
  2008: {
    KKR: 2,
    RCB: 1,
  },
  2010: {
    KKR: 1,
  },
  2017: {
    CSK: 1,
  },
};
test('Matches won per season', () => {
  expect(matchWon(sampleMatch)).toEqual(sampleOutput);
});
