function dismissal(b, deliveries) {
  let dismissed = {};
  for (let key of deliveries) {
    let dismissBat = key['player_dismissed'];
    let dismissBowl = key['bowler'];
    if (dismissBat != '') {
      dismissed[dismissBat + ',' + dismissBowl] =
        (dismissed[dismissBat + ',' + dismissBowl] || 0) + 1;
    }
  }
  return maxDismiss(dismissed);
}
function maxDismiss(dismissed) {
  let max = -Infinity;
  let dismiss = {};
  for (let key in dismissed) {
    if (dismissed[key] > max) {
      dismiss = {};
      max = dismissed[key];
      dismiss[key] = dismissed[key];
    } else if (dismissed[key] == max) {
      dismiss[key] = dismissed[key];
    }
  }
  return dismiss;
}
module.exports = dismissal;
