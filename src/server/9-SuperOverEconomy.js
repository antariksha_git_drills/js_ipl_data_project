function superOverEconomy(a, deliveries) {
  let totalRuns = [];
  let totalBalls = [];
  for (let matchId of deliveries) {
    if (matchId['is_super_over'] == 1) {
      if (matchId['bye_runs'] == 0 && matchId['legbye_runs'] == 0) {
        if (isNaN(totalRuns[matchId['bowler']])) {
          totalRuns[matchId['bowler']] = parseInt(matchId['total_runs']);
        } else {
          totalRuns[matchId['bowler']] += parseInt(matchId['total_runs']);
        }
      }
    }
  }
  for (let matchId of deliveries) {
    if (matchId['is_super_over'] == 1) {
      if (matchId['noball_runs'] == 0 && matchId['wide_runs'] == 0) {
        if (isNaN(totalBalls[matchId['bowler']])) {
          totalBalls[matchId['bowler']] = 1;
        } else {
          totalBalls[matchId['bowler']]++;
        }
      }
    }
  }
  var economy = {};
  for (var key in totalRuns) {
    economy[key] = (totalRuns[key] * 6) / totalBalls[key];
  }
  const sortedObject = Object.entries(economy).sort((x, y) => x[1] - y[1]);
  const sortedEconomy = sortedObject.reduce(
    (economy, [key, value]) => ({
      ...economy,
      [key]: value,
    }),
    {},
  );
  var slicedEco = Object.fromEntries(Object.entries(sortedEconomy).slice(0, 1));
  return slicedEco;
}

module.exports = superOverEconomy;
