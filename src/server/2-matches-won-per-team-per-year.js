function matchesWonPerTeamPerYear(matches) {
  var teams = {};
  for (let match of matches) {
    let team = match['season'];
    let matchWon = {};
    for (let prop of matches) {
      if (prop['season'] == team) {
        if (isNaN(matchWon[prop['winner']])) {
          matchWon[prop['winner']] = 1;
        } else {
          matchWon[prop['winner']]++;
        }
      }
    }
    teams[team] = matchWon;
  }

  return teams;
}
module.exports = matchesWonPerTeamPerYear;
