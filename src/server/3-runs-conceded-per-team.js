function extraRuns(matches, deliveries) {
  var extra = {};
  for (let key of matches) {
    if (key['season'] == 2016) {
      let id = key['id'];
      for (let matchId of deliveries) {
        if (matchId['match_id'] === id) {
          if (isNaN(extra[matchId['bowling_team']])) {
            extra[matchId['bowling_team']] = parseInt(matchId['extra_runs']);
          } else {
            extra[matchId['bowling_team']] += parseInt(matchId['extra_runs']);
          }
        }
      }
    }
  }

  return extra;
}

module.exports = extraRuns;
