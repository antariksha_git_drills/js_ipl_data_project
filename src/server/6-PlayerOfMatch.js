function PlayerOfMatch(matches) {
  const result = {};
  for (let match of matches) {
    result[match.season] = result[match.season] || {};
    result[match.season][match['player_of_match']] =
      (result[match.season][match['player_of_match']] || 0) + 1;
  }
  for (let season in result) {
    result[season] = maxValueKey(result[season]);
  }
  return result;
}

function maxValueKey(obj) {
  let maxi = -Infinity;
  let string = '';
  for (let key in obj) {
    if (maxi < obj[key]) {
      maxi = obj[key];
      string = key;
    } else if (maxi == obj[key]) {
      string += ', ' + key;
    }
  }
  return string;
}

module.exports = PlayerOfMatch;
