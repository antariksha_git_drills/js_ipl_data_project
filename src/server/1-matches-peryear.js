function matchPerYear(matches) {
  let result = {};
  for (let value of matches) {
    if (isNaN(result[value['season']])) {
      result[value['season']] = 1;
    } else {
      result[value['season']]++;
    }
  }
  return result;
}
module.exports = matchPerYear;
