const calculateBatsmanStrikeRate = function (matches, deliveries) {
  const result = {};

  deliveries.forEach((delivery) => {
    const matchId = delivery.match_id;
    const batsman = delivery.batsman;
    const runsScored = +delivery.batsman_runs;
    const isWideBall = delivery.wide_runs !== '0';

    const season = matches.find((match) => match.id === matchId)?.season;

    if (!season || isWideBall) {
      return;
    }

    if (!result[season]) {
      result[season] = {};
    }

    if (!result[season][batsman]) {
      result[season][batsman] = {
        runs: 0,
        ballsFaced: 0,
        strikeRate: 0,
      };
    }
    result[season][batsman].runs += runsScored;
    result[season][batsman].ballsFaced += 1;

    const totalBallsFaced = result[season][batsman].ballsFaced;
    const totalRunsScored = result[season][batsman].runs;

    if (totalBallsFaced > 0) {
      result[season][batsman].strikeRate = (
        (totalRunsScored / totalBallsFaced) *
        100
      ).toFixed(2);
    }
  });

  return result;
};

module.exports = calculateBatsmanStrikeRate;
