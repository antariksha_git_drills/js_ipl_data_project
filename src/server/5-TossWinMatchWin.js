function TossWinMatchWin(matches) {
  let teams = {};
  for (let key of matches) {
    if (key['toss_winner'] == key['winner']) {
      if (isNaN(teams[key['toss_winner']])) teams[key['toss_winner']] = 1;
      else {
        teams[key['toss_winner']]++;
      }
    }
  }
  return teams;
}

module.exports = TossWinMatchWin;
