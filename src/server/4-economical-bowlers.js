function Economy(matches, deliveries) {
  let totalRuns = {};
  let totalBalls = {};
  for (let key of matches) {
    if (key['season'] == 2015) {
      let id = key['id'];
      for (let matchId of deliveries) {
        if (matchId['bye_runs'] == 0 && matchId['legbye_runs'] == 0) {
          if (matchId['match_id'] == id) {
            if (isNaN(totalRuns[matchId['bowler']])) {
              totalRuns[matchId['bowler']] = parseInt(matchId['total_runs']);
            } else {
              totalRuns[matchId['bowler']] += parseInt(matchId['total_runs']);
            }
          }
        }
      }
    }
  }
  for (let key of matches) {
    if (key['season'] == 2015) {
      let id = key['id'];
      for (let matchId of deliveries) {
        if (matchId['match_id'] == id) {
          if (matchId['noball_runs'] == 0 && matchId['wide_runs'] == 0) {
            if (isNaN(totalBalls[matchId['bowler']])) {
              totalBalls[matchId['bowler']] = 1;
            } else {
              totalBalls[matchId['bowler']]++;
            }
          }
        }
      }
    }
  }

  let economy = {};
  for (let key in totalRuns) {
    economy[key] = (totalRuns[key] * 6) / totalBalls[key];
  }
  const sortedObject = Object.entries(economy).sort((x, y) => x[1] - y[1]);
  const sortedEconomy = sortedObject.reduce(
    (economy, [key, value]) => ({
      ...economy,
      [key]: value,
    }),
    {},
  );

  var topEco = Object.fromEntries(Object.entries(sortedEconomy).slice(0, 10));
  return topEco;
}

module.exports = Economy;
