/* eslint-disable @typescript-eslint/no-var-requires */
const csv = require('csv-parser');
const fs = require('fs');

//import
const extraRuns = require('./src/server/3-runs-conceded-per-team.js');
const matchPerYear = require('./src/server/1-matches-peryear.js');
const matchesPerTeam = require('./src/server/2-matches-won-per-team-per-year.js');
const ecoBowl = require('./src/server/4-economical-bowlers.js');
const tossWinMatchWin = require('./src/server/5-TossWinMatchWin.js');
const playerOfMatch = require('./src/server/6-PlayerOfMatch.js');
const superOverEco = require('./src/server/9-SuperOverEconomy.js');
const strikeRate = require('./src/server/7-StrikeRatePerSeason.js');
const dismissal = require('./src/server/8-HighestDismissal.js');

//calling
csvReadJsonWrite(extraRuns, '3-runsConceded');
csvReadJsonWrite(matchPerYear, '1-matches-peryear');
csvReadJsonWrite(matchesPerTeam, '2-matchesPerTeamPerYear');
csvReadJsonWrite(ecoBowl, '4-topEconomy');
csvReadJsonWrite(tossWinMatchWin, '5-tossAndMatchWin');
csvReadJsonWrite(playerOfMatch, '6-PlayerofMatch');
csvReadJsonWrite(superOverEco, '9-SuperOverEconomy');
csvReadJsonWrite(strikeRate, '7-StrikeRate');
csvReadJsonWrite(dismissal, '8-dismiss');

function csvReadJsonWrite(cb, path) {
  const matches = [];
  const deliveries = []; // Declare the deliveries array

  fs.createReadStream('./src/data/matches.csv')
    .pipe(csv({}))
    .on('data', (data) => matches.push(data))
    .on('end', () => {
      fs.createReadStream('./src/data/deliveries.csv')
        .pipe(csv({}))
        .on('data', (data) => deliveries.push(data)) // Push data into the deliveries array
        .on('end', () => {
          fs.writeFile(
            `./src/public/output/${path}.json`,
            JSON.stringify(cb(matches, deliveries), null, 2), // Pass both resultsMatches and deliveries to the callback
            (err) => {
              if (err) throw err;
              console.log('The file has been saved!');
            },
          );
        });
    });
}
